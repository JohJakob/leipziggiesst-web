
![Screenshot of _Leipzig giesst_](./docs/images/screenshot.png)

## About [LeipzigGiesst](https://www.leipziggiesst.de)

The consequences of climate change, especially the dry and hot summers, are putting a strain on Leipzig's ecosystem. Our urban trees are drying out and suffering long-term damage: In recent years, more and more trees have had to be cut down and their lifespan is declining. In the meantime, the population is regularly called upon to help, but largely uncoordinated. [_Leipzig giesst_](https://www.leipziggiesst.de) was made to change that and enable coordinated citizen* participation in the irrigation of urban trees. This project has been derived from [_GiessDenKiez_](https://www.giessdenkiez.de), that was made by the [Technologiestiftung Berlin](https://www.technologiestiftung-berlin.de/de/startseite/) and the [CityLAB Berlin](https://www.citylab-berlin.org/).

---

## Repositories

This project is composed of multiple repositories:

- [React frontend (this is here)](https://gitlab.com/leipziggiesst/web)
- [DWD and Tree Harvester](https://gitlab.com/leipziggiesst/treedata)
