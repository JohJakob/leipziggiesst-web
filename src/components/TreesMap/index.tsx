import React, { FC, useRef, useCallback, useState, useEffect } from 'react';
import { isMobile } from 'react-device-detect';
import debounce from 'lodash.debounce';
import { useActions, useStoreState } from '../../state/unistore-hooks';
import { useTreeData } from '../../utils/hooks/useTreeData';
import { useWaterSourceData } from '../../utils/hooks/useWaterSourceData';
import { useCommunityData } from '../../utils/hooks/useCommunityData';
import { useRainGeoJson } from '../../utils/hooks/useRainGeoJson';
import { useCurrentTreeId } from '../../utils/hooks/useCurrentTreeId';
import { useCurrentWaterSourceId } from '../../utils/hooks/useCurrentWaterSourceId';
import { usePumpsGeoJson } from '../../utils/hooks/usePumpsGeoJson';
import { useWaterSourcesGeoJson } from '../../utils/hooks/useWaterSourcesGeoJson';
import { useEventsGeoJson } from '../../utils/hooks/useEventsGeoJson';
import { useWoodsGeoJson } from '../../utils/hooks/useWoodsGeoJson';
import { useTreesArrow } from '../../utils/hooks/useTreesArrow';
import { tableFromIPC } from 'apache-arrow';
import 'mapbox-gl/dist/mapbox-gl.css';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
import { Map as MaplibreMap } from 'maplibre-gl';

const TreesMap = dynamic(() => import('./TreesMap'), {
  ssr: false
});

export const Map: FC<{
  showOverlay: boolean | undefined;
  isNavOpened: boolean | undefined;
}> = ({ showOverlay, isNavOpened }) => {
  const deckRef = useRef(null);
  const visibleMapLayer = useStoreState('visibleMapLayer');
  const ageRange = useStoreState('ageRange');
  const mapViewFilter = useStoreState('mapViewFilter');
  const mapWaterNeedFilter = useStoreState('mapWaterNeedFilter');
  const mapFocusPoint = useStoreState('mapFocusPoint');

  const { openNav, closeNav } = useActions();
  const { data: communityData } = useCommunityData();
  const { isLoading: isLoadingRainGeojson, data: rainGeoJson } = useRainGeoJson();
  const { isLoading: isLoadingPumpsGeojson, data: pumpsGeoJson } = usePumpsGeoJson();
  const { isLoading: isLoadingWaterSourcesGeojson, data: waterSourcesGeoJson } = useWaterSourcesGeoJson();
  const { isLoading: isLoadingEventsGeojson, data: eventsGeoJson } = useEventsGeoJson();
  const { isLoading: isLoadingWoodsGeojson, data: woodsGeoJson } = useWoodsGeoJson();

  const { isLoading: isLoadingTreesArrow, data: treesArrow } = useTreesArrow();
  const treeId = useCurrentTreeId();
  const waterSourceId = useCurrentWaterSourceId();
  const { treeData: selectedTreeData } = useTreeData(treeId);
  const { waterSourceData: selectedWaterSourceData } = useWaterSourceData(waterSourceId);
  const [initialCountSet, setInitialCountSet] = useState(false)
  const [initialCount, setInitialCount] = useState({
    treeCount: 0,
    waterSourceCount: 0,
    mobileCount: 0,
    pumpCount: 0 
  });
  const [count, setCount] = useState({
    treeCount: 0,
    pumpCount: 0, 
    waterSourceCount: 0,
    mobileCount: 0
  });
  const [zoom, setZoom] = useState(isMobile ? 13 : 11);
  const mapRef = useRef<MaplibreMap | null>(null);

  const { push } = useRouter();

  const pumpFilter = (feature) => feature?.properties?.type === 'Handschwengelpumpe';
  const mobileFilter = (feature) => feature?.properties?.type === 'LEIPZIG GIESST-Mobil';
  const waterSourcesFilter = (feature) => !pumpFilter(feature) && !mobileFilter(feature);

  useEffect(() => {
    if (!isLoadingTreesArrow && treesArrow) {
      treesArrow.arrayBuffer().then(arrayBuffer =>
        setInitialCount(old => ({
          ...old,
          treeCount: tableFromIPC(arrayBuffer).numRows
        }))); 
    }
  }, [isLoadingTreesArrow, treesArrow]);


  useEffect(() => {
    if (!initialCountSet) {
      if (!isLoadingWaterSourcesGeojson && waterSourcesGeoJson && !initialCountSet) {
        const features = (waterSourcesGeoJson as any).features;
        if (features) {
          const values = {
            waterSourceCount: features.filter(waterSourcesFilter).length,
            mobileCount: features.filter(mobileFilter).length,
            pumpCount: features.filter(pumpFilter).length,
          };
          setInitialCount(old => ({
            ...old,
            ...values
          })); 
          setCount(old => ({
            ...old,
            ...values
          })); 
          setInitialCountSet(true);
        }
      }
    }
  }, [isLoadingWaterSourcesGeojson, waterSourcesGeoJson]);

  const debouncedSet = debounce(({deck}) => {
      if (!initialCountSet) {
        return;
      }
      const zoomLevels = deck.deck.viewManager.getViewports().map(vp => vp.zoom)
      const zoomLevel = zoomLevels.length > 0 ? zoomLevels[0] : 15;
      const getFeatureCount = () => {
            const features = (layerIds) => deck.pickObjects({
              x: 0,
              y: 0,
              width: deck.deck.width,
              height: deck.deck.height,
              layerIds
            }).map(feature => feature.object);
            const treesFeature = features(['trees']).length;
            const waterSourceFeatures = features(['waterSources'])
            const values = {
              treeCount: treesFeature,
              waterSourceCount: waterSourceFeatures.filter(waterSourcesFilter).length,
              mobileCount: waterSourceFeatures.filter(mobileFilter).length,
              pumpCount: waterSourceFeatures.filter(pumpFilter).length,
            };
            setCount(old => ({
              ...old,
              ...values
            })); 
      };
      if (zoomLevel >= 15) {
        getFeatureCount();
      } else {
        setCount(old => ({
          ...old,
          ...initialCount
        })); 
      }
    }, 500);

  const onViewStateChanged = () => {
    if (deckRef.current) {
      const deck = deckRef.current;
      debouncedSet({ deck });
    }
  };

  return (
    <TreesMap
      map={mapRef}
      deckRef={deckRef}
      count={count}
      handleViewStateChanged={onViewStateChanged}
      zoom={zoom}
      setZoom={setZoom}
      onTreeSelect={id => {
        if (!id) {
          void push('/');
          closeNav();
          return;
        }
        const nextLocation = `/tree/${id}`;
        void push(nextLocation);
        openNav();
      }}
      onWaterSourceSelect={(id: string) => {
        const nextLocation = `/watersource/${id}`;
        void push(nextLocation);
        openNav();
      }}
      rainGeojson={rainGeoJson || null}
      waterSourcesGeoJson={waterSourcesGeoJson || null}
      eventsGeoJson={eventsGeoJson || null}
      woodsGeoJson={woodsGeoJson || null}
      visibleMapLayer={visibleMapLayer}
      isNavOpen={!!isNavOpened}
      showControls={showOverlay}
      pumpsGeoJson={pumpsGeoJson || null}
      isLoadingTreesArrow={isLoadingTreesArrow}
      treesArrow={treesArrow || null}
      ageRange={ageRange || []}
      mapViewFilter={mapViewFilter}
      mapWaterNeedFilter={mapWaterNeedFilter}
      communityData={communityData?.communityFlagsMap || null}
      communityDataWatered={communityData?.wateredTreesIds || []}
      communityDataAdopted={communityData?.adoptedTreesIds || {}}
      selectedTreeId={treeId || undefined}
      selectedWaterSourceId={waterSourceId || undefined}
      focusPoint={selectedTreeData || selectedWaterSourceData || mapFocusPoint}
    />
  );
};
