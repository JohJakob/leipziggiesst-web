import { easeCubic as d3EaseCubic, ExtendedFeatureCollection, TypedArray } from 'd3';
import React, { forwardRef, useCallback, useEffect, useRef, useState } from 'react';
import { Map as MaplibreMap } from 'maplibre-gl';
import Map, {
  GeolocateControl, MapRef, NavigationControl,
} from 'react-map-gl/maplibre';
import 'maplibre-gl/dist/maplibre-gl.css'
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import { CommunityDataType, StoreProps } from '../../common/interfaces';
import DeckGL, { GeoJsonLayer, IconLayer, RGBAColor } from 'deck.gl';
import { pumpEventInfoToState, PumpEventInfoType } from './pumpsUtils';
import { getTreeCircleColor, pumpToColor } from './mapColorUtil';
import { rgbStrToRgb, interpolateColor } from '../../utils/colorUtil';
import styled from 'styled-components';
import { isMobile } from 'react-device-detect';
import { MapTooltip } from './MapTooltip';
import { getOSMEditorURL } from './osmUtil';
import {
  getTreeCircleRadius,
  updateTreeCirclePaintProps,
} from './mapPaintPropsUtils';
import {
  updateHoverFeatureState,
  updateSelectedTreeIdFeatureState,
} from './mapFeatureStateUtil';
import { setBodyMapLayerClass } from './mapCSSClassUtil';
import { getFilterMatchingIdsList } from './mapboxGLExpressionsUtils';
import {
  YOUNG_TREE_MAX_AGE,
  OLD_TREE_MIN_AGE,
  LOW_WATER_NEED_NUM,
  MEDIUM_WATER_NEED_NUM,
  HIGH_WATER_NEED_NUM,
} from '../../utils/getWaterNeedByAge';
import { useActions, useStoreState } from '../../state/unistore-hooks';
import { GeoArrowPickingInfo, GeoArrowScatterplotLayer } from "@geoarrow/deck.gl-layers";
import { Table, tableFromIPC, makeVector, Vector, Data, FixedSizeList, Uint8, Field, makeData, makeBuilder, vectorFromArray, FixedSizeListBuilder, tableFromArrays, makeTable, Uint8Builder, Int, Int8Builder, Int8, IntBuilder } from 'apache-arrow';
import StatusBar, { Counts } from '../StatusBar';
import type { Accessor, Color } from "@deck.gl/core/typed";

const VIEWSTATE_TRANSITION_DURATION = 1000;
const VIEWSTATE_ZOOMEDIN_ZOOM = 19;

const colors = {
  transparent: [0, 0, 0, 0] as [number, number, number, number],
  blue: [53, 117, 177, 200] as [number, number, number, number],
  turquoise: [0, 128, 128, 200] as [number, number, number, number],
  greygreen: [126, 142, 128, 200] as [number, number, number, number],
  lightgreygreen: [188, 208, 191, 200] as [number, number, number, number],
};

interface StyledProps {
  isNavOpen?: boolean;
}

const ControlWrapper = styled.div<StyledProps>`
  position: fixed;
  bottom: 12px;
  left: 12px;
  z-index: 2;
  transition: transform 500ms;

  @media screen and (min-width: ${p => p.theme.screens.tablet}) {
    transform: ${props =>
      props.isNavOpen ? 'translate3d(350px, 0, 0)' : 'translate3d(0, 0, 0)'};
  }

  & > div {
    position: static !important;
  }
`;

const StyledTextLink = styled.a`
  color: black;
`;

interface TreesMapPropsType {
  deckRef: any;
  map: any;
  handleViewStateChanged: any;
  count: Counts;
  zoom: any;
  setZoom: any;
  rainGeojson: ExtendedFeatureCollection | null;

  visibleMapLayer: StoreProps['visibleMapLayer'];
  ageRange: StoreProps['ageRange'];
  mapViewFilter: StoreProps['mapViewFilter'];
  mapWaterNeedFilter: StoreProps['mapWaterNeedFilter'];
  isNavOpen: StoreProps['isNavOpen'];
  focusPoint: StoreProps['mapFocusPoint'];

  pumpsGeoJson: ExtendedFeatureCollection | null;
  waterSourcesGeoJson: ExtendedFeatureCollection | null;
  eventsGeoJson: ExtendedFeatureCollection | null;
  woodsGeoJson: ExtendedFeatureCollection | null;
  isLoadingTreesArrow: boolean;
  treesArrow: Blob | null;
  selectedTreeId: string | undefined;
  selectedWaterSourceId: string | undefined;
  communityData: CommunityDataType['communityFlagsMap'];
  communityDataWatered: CommunityDataType['wateredTreesIds'];
  communityDataAdopted: CommunityDataType['adoptedTreesIds'];

  showControls: boolean | undefined;
  onTreeSelect: (id?: string | null) => void;
  onWaterSourceSelect: (id: string) => void;
}

interface TypedViewportType {
  latitude: number;
  longitude: number;
  zoom: number;
  transitionDuration: number;
}


interface PumpPropertiesType {
  id?: number;
  // address: string;
  name: string;
  drinkable: string;
  // status: string;
  // check_date: string;
  // style: string;
}

interface PumpTooltipType extends PumpPropertiesType {
  x: number;
  y: number;
}

const [minLng, minLat, maxLng, maxLat] = (
  process.env.NEXT_PUBLIC_MAP_BOUNDING_BOX || ''
)
  .split(',')
  .map(coord => parseFloat(coord));

if (
  typeof maxLat !== 'number' ||
  typeof minLng !== 'number' ||
  typeof minLat !== 'number' ||
  typeof maxLng !== 'number'
) {
  throw new Error(`
    The environment variable NEXT_PUBLIC_MAP_BOUNDING_BOX was either missing or malformed.
    Please refer to the env.sample file for more info.
  `);
}

let hasUnmounted = false;
const TreesMap = forwardRef<MapRef, TreesMapPropsType>(function TreesMap(
  {
    rainGeojson,
    visibleMapLayer,
    ageRange,
    mapViewFilter,
    mapWaterNeedFilter,
    isNavOpen,
    focusPoint,
    pumpsGeoJson,
    waterSourcesGeoJson,
    eventsGeoJson,
    woodsGeoJson,
    isLoadingTreesArrow,
    treesArrow,
    selectedTreeId,
    communityDataWatered,
    communityDataAdopted,
    showControls,
    onTreeSelect,
    deckRef, 
    map,
    handleViewStateChanged, 
    count, 
    zoom
  },
  ref
) {
  const lastSelectedTree = useRef<string | undefined>(selectedTreeId);
  const lastHoveredTreeId = useRef<string | null>(null);
  const [hoveredTreeId, setHoveredTreeId] = useState<string | null>(null);
  const [hoveredPump, setHoveredPump] = useState<PumpTooltipType | null>(null);
  const [clickedPump, setClickedPump] = useState<PumpTooltipType | null>(null);
  const [arrowProcessed, setArrowProcessed] = useState<GeoArrowScatterplotLayer | null>(null);
  const initialLatitude = Number(process.env.NEXT_PUBLIC_MAP_INITIAL_LATITUDE || 51.3399028);
  const initialLongitude = Number(process.env.NEXT_PUBLIC_MAP_INITIAL_LONGITUDE || 12.3742236);
  const initialZoom = Number(process.env.NEXT_PUBLIC_MAP_INITIAL_ZOOM || 11);
  const initialZoomMobile = Number(process.env.NEXT_PUBLIC_MAP_INITIAL_ZOOM_MOBILE || 13);
  const [cursor, setCursor] = useState('grab');

  const defaultViewport = {
    latitude: initialLatitude,
    longitude: initialLongitude,
    zoom: isMobile ? initialZoomMobile : initialZoom,
    maxZoom: VIEWSTATE_ZOOMEDIN_ZOOM,
    minZoom: 11,
    pitch: isMobile ? 0 : 45,
    bearing: 0,
    transitionDuration: 2000,
    transitionEasing: d3EaseCubic,
    //transitionInterpolator: new MapboxGeocoder(),
  };

  const [viewport, setViewport] = useState<TypedViewportType>(defaultViewport);
  const { setMapHasLoaded } = useActions();
  const mapHasLoaded = useStoreState('mapHasLoaded');

  useEffect(
    () => () => {
      hasUnmounted = true;
    },
    []
  );

  const calculateTreeColor = (arrowTable) => {
    const numberOrDefault = (val, def = 0) => isNaN(parseInt(val)) ? def : parseInt(val)
    const minFilteredAge = numberOrDefault(ageRange[0]);
    const maxFilteredAge = numberOrDefault(ageRange[1], 320);

    const ageFilterIsApplied =
      minFilteredAge !== 0 || maxFilteredAge !== 320; // TODO: how to not hard-code these values?

    const batches: Data<FixedSizeList<Uint8>>[] = [];
    var batchCount = 0;
    var radolan_sum_col = arrowTable.getChild("radolan_sum")
    if (radolan_sum_col) {
      radolan_sum_col.data.forEach(batch => {
        const fixedType = new FixedSizeList(4, new Field<Uint8>("item", new Uint8()));
        const builder = new FixedSizeListBuilder({ type: fixedType });
        const intBuilder = new Uint8Builder<Int>({type: new Uint8()});
        builder.addChild(intBuilder)
        var counter = 0;
        batch.values.forEach(value => {
          const radolan_sum = value / 10
          const id_child = arrowTable.getChild("id");
          const id = id_child && id_child.data[batchCount].values[counter];
          const wateredAmount = communityDataWatered && id && communityDataWatered[id];
          const isAdopted = communityDataAdopted && id && communityDataAdopted[id];
          const rainOrWaterDataExists = !!radolan_sum || !!wateredAmount;
          const age_child = arrowTable.getChild("age");
          if (age_child) {
            const treeAge = age_child.data[batchCount].values[counter];
            const treeIsWithinAgeRange =
              treeAge === undefined || (typeof treeAge === 'number' && treeAge >= minFilteredAge && treeAge <= maxFilteredAge);
            const treeIsWithinRelevantAgeRange =
              treeAge === undefined || (typeof treeAge === 'number' && treeAge >= 4 && treeAge <= 15);
            const colorsShallBeInterpolated =
              rainOrWaterDataExists &&
              treeIsWithinRelevantAgeRange &&
              ((ageFilterIsApplied && treeIsWithinAgeRange) ||
                !ageFilterIsApplied);      
            const colorShallBeTransparent =
              (ageFilterIsApplied && !treeIsWithinAgeRange) ||
              !rainOrWaterDataExists;
            var colorToUse;  
            if (colorShallBeTransparent) {
              colorToUse = colors.transparent;
            } else if (mapViewFilter === 'watered') {
              colorToUse = wateredAmount && wateredAmount > 0 && treeIsWithinAgeRange
                ? colors.blue
                : colors.transparent;
            } else if (mapViewFilter === 'adopted') {
              colorToUse = isAdopted && treeIsWithinAgeRange
                ? colors.turquoise
                : colors.transparent;
            } else if (colorsShallBeInterpolated) {
              // Note: we do check if radolan_sum is defined by checking for rainDataExists, that's why the ts-ignore
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              const waterSum = wateredAmount ? numberOrDefault(wateredAmount) : 0;
              const radolanSum = numberOrDefault(radolan_sum);
              const sum = radolanSum + waterSum;
              const interpolated = interpolateColor(sum);
              colorToUse = rgbStrToRgb(interpolated);
            } else if (treeAge < 5) {
              colorToUse = colors.lightgreygreen;
            } else if (treeAge > 15) {
              colorToUse = colors.greygreen;
            }
            builder.set(counter, Uint8Array.from([colorToUse[0], colorToUse[1], colorToUse[2], colorToUse[3]]));
            counter++;
          }
        })
        const data: Data<FixedSizeList<Uint8>> = builder.finish().flush();
        batches.push(data);
        batchCount++;
      });
    }
    const transformed: Vector<FixedSizeList<Uint8>> = makeVector(batches);
    return transformed;
  }

  const createTreeLayer = (arrowTable) => {
    const treeLayer = new GeoArrowScatterplotLayer({
      id: "trees",
      data: arrowTable,
      getFillColor: calculateTreeColor(arrowTable),
      radiusMinPixels: 1.5,
      getPointRadius: 10,
      pointRadiusMinPixels: 0.8,
      pickable: true,
      visible: visibleMapLayer.includes('rain') ? false : true,
    });

    treeLayer.onHover = (info: GeoArrowPickingInfo, pickingEvent: any) => {
      if (!map.current || hasUnmounted) return;
      if (pickingEvent.type === 'pointermove') {
        const id: string = info?.object?.id as string;
        setHoveredTreeId(id);
        return !!id;
      } else {
        setHoveredTreeId(null);
      }
    };
    setArrowProcessed(treeLayer);
    return treeLayer;
  }

  useEffect(
    () => {
      if (arrowProcessed) {
        deckRef.current?.deck?.layerManager.layers?.push(createTreeLayer(arrowProcessed.props.data))
      }
    },
    [ageRange]
  );

  const renderLayers = useCallback(() => {
    if (!map?.current || !rainGeojson || !pumpsGeoJson || hasUnmounted || isLoadingTreesArrow || !treesArrow) {
      return [];
    }

    if (!arrowProcessed) {
      treesArrow.arrayBuffer().then(arrayBuffer => {
        const arrowTable = tableFromIPC(arrayBuffer);
        createTreeLayer(arrowTable);
      });
    }

    const woodsLayer = new GeoJsonLayer({
      id: 'woods',
      data: woodsGeoJson as any,
      opacity: 1.0,
      visible: visibleMapLayer.indexOf('trees') >= 0 ? true : false,
      stroked: true,
      filled: false,
      extruded: true,
      wireframe: true,
      getElevation: 7,
      getLineColor: [0, 0, 0, 200],
      getFillColor: [215, 234, 229, 200],
      getRadius: 9,
      pointRadiusMinPixels: 4,
      pickable: false,
      lineWidthScale: 3,
      lineWidthMinPixels: 1.5,
    });

    const getEventsIcon = (d, prefix) => {
      return {
        url: prefix + "images/events.svg",
        width: 132,
        height: 132,
        anchorY: 32
      };
    }

    const getWaterSourceIcon = (d, prefix) => {
      if (d.properties.type == "Handschwengelpumpe") {
        return {
          url: prefix + "images/pumpe_64.png",
          width: 132,
          height: 132,
          anchorY: 32
        };
      } else if (d.properties.type == "Privatperson") {
        return {
          url: prefix + "images/drinking-water.png",
          width: 132,
          height: 132,
          anchorY: 32
        };
      } else if (d.properties.type == "Schöpfstelle") {
        return {
          url: prefix + "images/river-icon-clipart.png",
          width: 32,
          height: 32,
          anchorY: 16
        };
      } else if (d.properties.type == "LEIPZIG GIESST-Mobil") {
        return {
          url: prefix + "images/dumpster.png",
          width: 132,
          height: 132,
          anchorY: 32
        };
      } else {
        return {
          url: prefix + "images/drinking-water.png",
          width: 132,
          height: 132,
          anchorY: 32
        };
      }
    };

    const waterSourcesLayer = new IconLayer({
      id: 'waterSources',
      visible: visibleMapLayer.includes('water_sources') ? true : false,
      data: (waterSourcesGeoJson as any).features,
      pickable: true,
      // iconAtlas and iconMapping are required
      // getIcon: return a string
      getIcon: d => getWaterSourceIcon(d, window.location.pathname.split('/').filter(s => s.length > 0).map(_ => '../').join('')),
      sizeScale: zoom ? Math.pow(2, -12 + zoom) : 5,
      getPosition: (d) => d.geometry.coordinates,
      getSize: (d) => 1,
      getColor: (d) => [140, 140, 0],
      onClick: info => {
        this._onClick(info.x, info.y, info.object);
      },
    });


    const eventsLayer = new IconLayer({
      id: 'events',
      visible: visibleMapLayer.includes('events') ? true : false,
      data: (eventsGeoJson as any).features,
      pickable: true,
      // iconAtlas and iconMapping are required
      // getIcon: return a string
      getIcon: d => getEventsIcon(d, window.location.pathname.split('/').filter(s => s.length > 0).map(_ => '../').join('')),
      sizeScale: zoom ? Math.pow(2, -12 + zoom) : 5,
      getPosition: (d) => d.geometry.coordinates,
      getSize: (d) => 1,
      getColor: (d) => [140, 140, 0],
      onClick: info => {
        this._onClick(info.x, info.y, info.object);
      },
    });

    const rainLayer = new GeoJsonLayer({
      id: 'rain',
      data: rainGeojson as unknown,
      opacity: 0.95,
      visible: visibleMapLayer.includes('rain') ? true : false,
      stroked: false,
      filled: true,
      extruded: true,
      wireframe: true,
      getElevation: 1,
      getFillColor: (f?: { properties?: { data?: number[] } }): RGBAColor => {
        /**
         * Apparently DWD 1 is not 1ml but 0.1ml
         * We could change this in the database, but this would mean,
         * transferring 800.000 "," characters, therefore,
         * changing it client-side makes more sense.
         */
        const features = f?.properties?.data || [];
        if (features.length === 0) return [0, 0, 0, 0];
        const interpolated = interpolateColor(features[0] / 10);
        const hex = rgbStrToRgb(interpolated);
        return hex;
      },
      pickable: true,
    });

    const layers = arrowProcessed ? [ arrowProcessed, rainLayer, woodsLayer, waterSourcesLayer, eventsLayer ] : 
      [ rainLayer, woodsLayer, waterSourcesLayer, eventsLayer ];

    return layers as unknown[];
  }, [pumpsGeoJson, rainGeojson, visibleMapLayer, arrowProcessed, isLoadingTreesArrow, treesArrow, woodsGeoJson, waterSourcesGeoJson, eventsGeoJson, ageRange ]);

  const onViewStateChange = useCallback((newViewport: TypedViewportType) => {
    if (hasUnmounted || !newViewport) return;
    const newViewState = {
      ...defaultViewport,
      ...newViewport,
      transitionDuration: newViewport.transitionDuration || 0,
    };
    setClickedPump(null);
    setHoveredPump(null);
    setViewport({
      ...newViewState,
      latitude: Math.min(maxLat, Math.max(newViewState.latitude, minLat)),
      longitude: Math.min(maxLng, Math.max(newViewState.longitude, minLng)),
    });
    handleViewStateChanged("onViewStateChange");
  }, []);

  const onMapClick = useCallback(
    (info: GeoArrowPickingInfo) => {
      if (!map.current || hasUnmounted) return;

      if (!info) {
        onTreeSelect(null);
        return;
      }

      const id: string = info.object?.id as string;

      if (!id) return;

      onTreeSelect(id);
    },
    [onTreeSelect]
  );

  const onLoad = useCallback(
    (evt: { target: MaplibreMap }) => {
      map.current = evt.target;

      if (!map.current || typeof map.current === 'undefined' || hasUnmounted)
        return;

      setMapHasLoaded();

      const firstLabelLayerId = map.current
        .getStyle()
        .layers?.find(layer => layer.type === 'symbol')?.id;

      if (!firstLabelLayerId) return;

      map.current.addSource('openmaptiles', {
        url: `https://api.maptiler.com/tiles/v3/tiles.json?key=${process.env.NEXT_PUBLIC_MAPTILER_KEY}`,
        type: 'vector',
      });

      map.current.addLayer(
        {
          'id': '3d-buildings',
          'source': 'openmaptiles',
          'source-layer': 'building',
          'type': 'fill-extrusion',
              'minzoom': 15,
              'paint': {
                  'fill-extrusion-color': [
                      'interpolate',
                      ['linear'],
                      ['get', 'render_height'], 0, 'lightgray', 200, 'royalblue', 400, 'lightblue'
                  ],
                  'fill-extrusion-height': [
                      'interpolate',
                      ['linear'],
                      ['zoom'],
                      15,
                      0,
                      16,
                      ['get', 'render_height']
                  ],
                  'fill-extrusion-base': ['case',
                      ['>=', ['get', 'zoom'], 16],
                      ['get', 'render_min_height'], 0
                  ]
              }
          },
        firstLabelLayerId
      );

      // disable map rotation using right click + drag
      map.current.dragRotate.disable();

      // disable map rotation using touch rotation gesture
      map.current.touchZoomRotate.disableRotation();
/*
      map.current.addSource('trees', {
        type: 'vector',
        url: process.env.NEXT_PUBLIC_MAPBOX_TREES_TILESET_URL,
        minzoom: 0,
        maxzoom: 20,
        promoteId: 'id',
      });

      map.current.addLayer({
        id: 'trees',
        type: 'circle',
        source: 'trees',
        'source-layer': process.env.NEXT_PUBLIC_MAPBOX_TREES_TILESET_LAYER,
        minzoom: 0,
        paint: {
          'circle-pitch-alignment': 'map',
          'circle-radius': getTreeCircleRadius({}),
          'circle-opacity': [
            'interpolate',
            ['linear'],
            ['zoom'],
            0,
            1,
            20,
            0.5,
          ],
          'circle-stroke-color': [
            'case',
            ['boolean', ['feature-state', 'select'], false],
            'rgba(247, 105, 6, 1)',
            getTreeCircleColor(),
          ],
          'circle-color': getTreeCircleColor(),
          'circle-stroke-width': [
            'case',
            ['boolean', ['feature-state', 'select'], false],
            15,
            0,
          ],
        },
      });
*/
      map.current.on('zoom', e => {
        onViewStateChange(e.viewState);
      });

      updateSelectedTreeIdFeatureState({
        map: map.current,
        prevSelectedTreeId: undefined,
        currentSelectedTreeId: selectedTreeId,
      });

      for (const visibleMapLayerObj of visibleMapLayer) {
        setBodyMapLayerClass(visibleMapLayerObj);
      }

      if (!focusPoint) return;
      onViewStateChange({
        latitude: focusPoint.latitude,
        longitude: focusPoint.longitude,
        zoom: focusPoint.zoom || VIEWSTATE_ZOOMEDIN_ZOOM,
        transitionDuration: VIEWSTATE_TRANSITION_DURATION,
      });
    },
    [
      focusPoint,
      onViewStateChange,
      selectedTreeId,
      setMapHasLoaded,
      visibleMapLayer,
    ]
  );

  useEffect(() => {
    if (!map?.current || hasUnmounted || !mapHasLoaded || isLoadingTreesArrow) return;
    updateSelectedTreeIdFeatureState({
      map: map.current,
      prevSelectedTreeId: lastSelectedTree.current,
      currentSelectedTreeId: selectedTreeId,
    });
    lastSelectedTree.current = selectedTreeId;
  }, [selectedTreeId, mapHasLoaded, isLoadingTreesArrow]);

  useEffect(() => {
    if (!map?.current || hasUnmounted || !mapHasLoaded || isLoadingTreesArrow) return;
    updateHoverFeatureState({
      map: map.current,
      prevHoveredTreeId: lastHoveredTreeId.current,
      currentHoveredTreeId: hoveredTreeId,
    });
    lastHoveredTreeId.current = hoveredTreeId;
  }, [hoveredTreeId, mapHasLoaded, isLoadingTreesArrow]);

  useEffect(() => {
    for (const visibleMapLayerObj of visibleMapLayer) {
      setBodyMapLayerClass(visibleMapLayerObj);
    }

    if (!map?.current || hasUnmounted) return;
    if (!visibleMapLayer.includes('trees')) {
      map.current.setLayoutProperty('trees', 'visibility', 'none');
      return;
    } else {
      map.current.setLayoutProperty('trees', 'visibility', 'visible');
    }
  }, [visibleMapLayer]);

  useEffect(() => {
    if (
      !map?.current ||
      hasUnmounted ||
      !communityDataWatered.length ||
      Object.keys(communityDataAdopted).length === 0
    )
      return;

    updateTreeCirclePaintProps({
      map: map.current,
      wateredFilterOn: mapViewFilter === 'watered',
      adoptedFilterOn: mapViewFilter === 'adopted',
    });

    let communityFilter: boolean | unknown[] | null = null;
    let waterNeedFilter: boolean | unknown[] | null = null;
    if (mapViewFilter === 'watered') {
      communityFilter = getFilterMatchingIdsList(communityDataWatered);
    } else if (mapViewFilter === 'adopted') {
      communityFilter = getFilterMatchingIdsList(
        Object.keys(communityDataAdopted)
      );
    }
    if (mapWaterNeedFilter !== null) {
      waterNeedFilter = [
        'match',
        [
          'case',
          ['<', ['get', 'age'], OLD_TREE_MIN_AGE],
          [
            'case',
            ['<', ['get', 'age'], YOUNG_TREE_MAX_AGE],
            HIGH_WATER_NEED_NUM,
            MEDIUM_WATER_NEED_NUM,
          ],
          LOW_WATER_NEED_NUM,
        ],
        mapWaterNeedFilter,
        true,
        false,
      ];
    }

    map.current.setFilter(
      'trees',
      ['all', communityFilter, waterNeedFilter].filter(val => val !== null)
    );
  }, [
    communityDataAdopted,
    communityDataWatered,
    mapViewFilter,
    mapWaterNeedFilter,
  ]);

  useEffect(() => {
    if (!map?.current || hasUnmounted) return;
    setCursor(hoveredTreeId || hoveredPump ? 'pointer' : 'grab');
  }, [hoveredTreeId, hoveredPump]);

  useEffect(() => {
    if (!focusPoint?.id || hasUnmounted) return;
    onViewStateChange({
      latitude: focusPoint.latitude,
      longitude: focusPoint.longitude,
      zoom: focusPoint.zoom || VIEWSTATE_ZOOMEDIN_ZOOM,
      transitionDuration: VIEWSTATE_TRANSITION_DURATION,
    });
  }, [
    focusPoint?.latitude,
    focusPoint?.longitude,
    focusPoint?.zoom,
    focusPoint?.id,
    onViewStateChange,
  ]);

  return (
    <>
      <DeckGL
        ref={deckRef}
        layers={renderLayers()}
        viewState={viewport as unknown}
        onViewStateChange={(e: { viewState: TypedViewportType }) => {
          onViewStateChange(e.viewState);
        }}
        onClick={onMapClick}
        controller
        style={{ overflow: 'hidden' }}
        getCursor={() => cursor}
      >
        <Map
          reuseMaps
          ref={ref}
          mapStyle={process.env.NEXT_PUBLIC_MAPTILER_STYLE}
          onLoad={onLoad}
        >
          {!showControls && (
            <ControlWrapper isNavOpen={isNavOpen}>
              <GeolocateControl
                positionOptions={{ enableHighAccuracy: true }}
                trackUserLocation={isMobile ? true : false}
                showUserLocation={true}
                onGeolocate={(posOptions: {
                  coords: {
                    latitude: number;
                    longitude: number;
                  };
                }) => {
                  const { latitude, longitude } = posOptions.coords;
                  onViewStateChange({
                    longitude,
                    latitude,
                    zoom: VIEWSTATE_ZOOMEDIN_ZOOM,
                    transitionDuration: VIEWSTATE_TRANSITION_DURATION,
                  });
                }}
              />
              <NavigationControl showCompass={false}/>
            </ControlWrapper>
          )}
        </Map>
      </DeckGL>
      <StatusBar
          count={count}
        />
    </>
  );
});

export default TreesMap;