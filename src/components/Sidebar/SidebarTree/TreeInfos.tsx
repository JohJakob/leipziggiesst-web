import React, { FC, useMemo, useState } from 'react';
import styled from 'styled-components';
import { getWaterNeedByAge } from '../../../utils/getWaterNeedByAge';
import ShareIcon from '@mui/icons-material/Share';
import FileCopyIcon from '@mui/icons-material/FileCopy';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import copy from "copy-to-clipboard";

import ExpandablePanel from '../../ExpandablePanel';
import WaterNeedsInfo from '../../WaterNeedsInfo';
import UsersWateringsList from '../../UsersWateringsList';
import ButtonWater from '../../ButtonWater';
import WaterDrops from '../../WaterDrops';
import Login from '../../Login';

import content from '../../../assets/content';
import { SelectedTreeType } from '../../../common/interfaces';
import Icon from '../../Icons';
import StackedBarChart from '../../StackedBarChart';
import { useUserData } from '../../../utils/hooks/useUserData';
import { ParticipateButton } from '../../ParticipateButton';
import Paragraph from '../../Paragraph';
import { NonVerfiedMailMessage } from '../../NonVerfiedMailMessage';
import ButtonRound from '../../ButtonRound';
import SmallParagraph from '../../SmallParagraph';
import { useAdoptingActions } from '../../../utils/hooks/useAdoptingActions';
import { useCommunityData } from '../../../utils/hooks/useCommunityData';
import { rainCircle, wateredCircle } from '../../StackedBarChart/TooltipLegend';

const logoWikipedia = '/images/Wikipedia-W-bold-in-square-Clean_(alt_crop).svg';
const logoWikicommons = '/images/Green_circle_with_camera_and_white_border.svg';
const logoWikidata = '/images/Wikidata-HS-icon.svg';
const logoInfo = '/images/Information_icon_1(png).png'

const steckbriefe = {
  "Tilia": "https://www.bund-leipzig.de/fileadmin/leipzig/PDF/Steckbrief/Baumsteckbriefe.pdf",
  "Betula": "https://www.bund-leipzig.de/fileadmin/leipzig/PDF/Steckbrief/Baumsteckbriefe.pdf#page=7",
  "Salix": "https://www.bund-leipzig.de/fileadmin/leipzig/PDF/Steckbrief/Baumsteckbriefe.pdf#page=8",
  "Castanea": "https://www.bund-leipzig.de/fileadmin/leipzig/PDF/Steckbrief/Baumsteckbriefe.pdf#page=6",
  "Quercus": "https://www.bund-leipzig.de/fileadmin/leipzig/PDF/Steckbrief/Baumsteckbriefe.pdf#page=5",
  "Platanus": "https://www.bund-leipzig.de/fileadmin/leipzig/PDF/Steckbrief/Baumsteckbriefe.pdf#page=4",
  "Acer": "https://www.bund-leipzig.de/fileadmin/leipzig/PDF/Steckbrief/Baumsteckbriefe.pdf#page=2",
  "Fraxinus": "https://www.bund-leipzig.de/fileadmin/leipzig/PDF/Steckbrief/Baumsteckbriefe.pdf#page=3"
}

const WikipediaLogo = styled.img`
  padding-bottom: 6px;
  height: 15px;
`;

const WikicommonsLogo = styled.img`
  padding-bottom: 2px;
  height: 25px;
`;

const WikidataLogo = styled.img`
  height: 30px;
`;

const InfoLogo = styled.img`
  padding-bottom: 4px;
  height: 20px;
`;

const { treetypes } = content.sidebar;

const Wrapper = styled.div`
  z-index: 3;
  margin: 0 0 20px;
`;

const FlexColumnDiv = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const CaretakerDiv = styled.div`
  padding-bottom: 0.75rem;
  display: flex;
  flex-direction: row;
  justify-content: start;
  align-items: center;
  line-height: ${p =>
    parseFloat(p.theme.fontSizeM.replace('rem', '')) * 1.2}rem;
`;

const CaretakerSublineSpan = styled.span`
  font-size: ${p => p.theme.fontSizeM};
  margin-top: -${p => parseFloat(p.theme.fontSizeM.replace('rem', '')) / 2 + 0.1}rem;
  margin-left: -${p => p.theme.fontSizeM};
`;

const SublineSpan = styled.span`
  margin-bottom: 0.75rem;
  text-transform: capitalize;
`;

const TreeTitle = styled.h2`
  font-size: 24px;
  font-weight: 600;
  margin-top: 0px;
  line-height: 125%;
  margin-bottom: 5px;
`;


const InfoContainer = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid ${p => p.theme.colorGreyLight};
  padding: 12px 0;
  font-weight: bold;
`;

const InfoValue = styled.span`
  font-weight: normal;
`;

const AdoptedIndication = styled.span<{
  selfAdopted?: boolean;
}>`
  display: inline-block;
  border-radius: 2px;
  font-size: ${p => p.theme.fontSizeM};
  line-height: ${p => p.theme.fontSizeM};
  color: ${p =>
    p.selfAdopted ? p.theme.colorPrimary : p.theme.colorSecondary};
  border: 1px solid;
  padding: 4px 5px;
  font-weight: normal;
  transform: translateY(-4px);
`;

const AdoptionsParent = styled.div`
  display: flex;
  column-gap: 8px;
  row-gap: 4px;
  margin-top: 8px;
  flex-wrap: wrap;
`;

const ActionsWrapper = styled.div`
  padding-top: ${p => p.theme.spacingM};
`;

const BaselineGrid = styled.span`
  display: inline-grid;
  grid-auto-flow: column;
  align-items: baseline;
  column-gap: 4px;
`;

const TreeInfos: FC<{
  selectedTreeData: SelectedTreeType;
}> = ({ selectedTreeData }) => {
  const {
    id: treeId,
    pflanzjahr,
    standortnr,
    artbot,
    artdtsch,
    gattung,
    gattungdeutsch,
    gattungwikipedia,
    gattungwikidata,
    gattungwikicommons,
    artwikipedia,
    artwikidata,
    artwikicommons,
    baumhoehe,
    kronedurch,
    stammumfg,
    kennzeich,
    eigentuemer,
    gebiet,
    status_patenbaum,
    patenschaftsnummer,
    letzte_bewaesserung,
    caretaker,
    waterings,
    notes,
  } = selectedTreeData;

  const steckbrief = gattung && steckbriefe[gattung];

  const [open, setOpen] = useState(false);

  const { userData } = useUserData();
  const {
    unadoptTree,
    adoptTree,
    isBeingAdopted,
    isBeingUnadopted,
  } = useAdoptingActions(treeId);
  const { data: communityData } = useCommunityData();

  const treeType = treetypes.find(treetype => treetype.id === gattungdeutsch);

  const adoptedByLoggedInUser =
    userData && userData.adoptedTrees.find(({ id }) => id === treeId);

  const adoptionsMap = communityData?.adoptedTreesIds || {};
  const numAdoptionsByOtherUsers = adoptionsMap[treeId] || 0;
  const adoptedByOtherUsers =
    (adoptedByLoggedInUser
      ? numAdoptionsByOtherUsers - 1
      : numAdoptionsByOtherUsers) > 0;

  const treeAge =
    pflanzjahr && pflanzjahr !== 'undefined' && pflanzjahr !== 'NaN'
      ? new Date().getFullYear() - parseInt(pflanzjahr, 10)
      : undefined;

  const getTreeLink = () => window.location.href;

  const currentDate = new Date();
  const todayAt3pm = new Date(new Date().setHours(15));
  /*
    We force the bar chart date to be 15:00 today, so that the new timestamped waterings (which are all at 15:00) will be displayed immediately. After 15:00 we can use the current time again.
  */
  const barChartDate = currentDate < todayAt3pm ? todayAt3pm : currentDate;

  const wateringsSum = useMemo(() => {
    if (!waterings) return 0;
    const last30DaysWaterings = waterings.filter(w => {
      const msPerDay = 86400000;
      const elapsedMs = new Date().getTime() - Date.parse(w.timestamp);
      return elapsedMs / msPerDay <= 30;
    });
    return last30DaysWaterings.reduce(
      (sum, current) => sum + current.amount,
      0
    );
  }, [waterings]);

  const rainSum = useMemo(() => {
    // reverse because last element is most recent rain
    const last30Days = [...selectedTreeData.radolan_days]
      .reverse()
      .slice(-(30 * 24));
    return last30Days.reduce((sum, current) => sum + current, 0);
  }, [selectedTreeData.radolan_days]);


  const handleLink = async () => {
    if (navigator.share) {
      await navigator.share({
        title: 'Baum-Link',
        text: 'Teile den Link zum Baum',
        url: getTreeLink()
      })
      .catch(console.error);
    } else {
      setOpen(true)
    }
  };

  const formatNumber = (str: string) => {
    try {
      return parseFloat(str).toLocaleString("de");
    } catch(e) {
      return str;
    }
  };

  return (
    <Wrapper>
      <Dialog onClose={() => setOpen(false)} aria-labelledby="share-tree-dialog-title" open={open}>
        <DialogTitle id="share-tree-dialog-title">Baum-Link</DialogTitle>
        <DialogContent>
          <DialogContentText>Teile den Link zum Baum:</DialogContentText>
          <DialogContentText>
            <a href={`${getTreeLink()}`}>{getTreeLink()}</a>
            <IconButton onClick={() => copy(getTreeLink())}>
              <FileCopyIcon />
            </IconButton>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} color="primary">
            Schließen
          </Button>
        </DialogActions>
      </Dialog>
      <FlexColumnDiv>
        {(artdtsch || gattungdeutsch || treeType?.title) && (
          <TreeTitle>
            {artdtsch || gattungdeutsch || treeType?.title}
            <IconButton onClick={handleLink}><ShareIcon /></IconButton>
          </TreeTitle>
        )}
        {(adoptedByLoggedInUser || adoptedByOtherUsers) && (
          <AdoptionsParent>
            {adoptedByLoggedInUser && (
              <AdoptedIndication selfAdopted>
                Von mir adoptiert ✔
              </AdoptedIndication>
            )}
            {adoptedByOtherUsers && (
              <AdoptedIndication>
                {adoptedByLoggedInUser
                  ? `Ebenfalls von anderen adoptiert`
                  : `Von anderen Nutzer:innen adoptiert`}{' '}
                ✔
              </AdoptedIndication>
            )}
          </AdoptionsParent>
        )}
        {caretaker && caretaker.length > 0 && (
          <CaretakerDiv>
            <Icon iconType='water' height={32}></Icon>
            <CaretakerSublineSpan>{`Dieser Baum wird regelmäßig vom ${caretaker} gewässert.`}</CaretakerSublineSpan>
          </CaretakerDiv>
        )}
        {treeType && treeType.title && (
          <ExpandablePanel title={treeType.title}>
            {treeType.description}
          </ExpandablePanel>
        )}
        {(artwikipedia || artwikicommons || artwikidata) && (
          <InfoContainer>
            <span style={{ paddingTop: "8px" }}>Baumart-Infos</span>
            <InfoValue>
            <div style={{ height: "40px" }}>
                {artwikipedia && (
                  <a
                    href={artwikipedia}
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <WikipediaLogo src={logoWikipedia} alt='Link zu Wikipedia-Eintrag' />
                  </a>
                )}
                {artwikicommons && (
                  <a
                    href={artwikicommons}
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <WikicommonsLogo src={logoWikicommons} alt='Link zu Wikicommons-Eintrag' />
                  </a>
                )}
                {artwikidata && (
                  <a
                    href={artwikidata}
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <WikidataLogo src={logoWikidata} alt='Link zu Wikidata-Eintrag' />
                  </a>
                )}
              </div>
            </InfoValue>
          </InfoContainer>
        )}
        {artbot && (
          <InfoContainer>
            <span>Name (wiss.)</span>
            <InfoValue>{artbot}</InfoValue>
          </InfoContainer>
        )}
        {gattungdeutsch && (
          <InfoContainer>
            <span>Gattung</span>
            <InfoValue>
              {gattungdeutsch}
            </InfoValue>
          </InfoContainer>
        )}
        {(gattungwikipedia || gattungwikicommons || gattungwikidata) && (
          <InfoContainer>
            <span style={{ paddingTop: "8px" }}>Gattung-Infos</span>
            <InfoValue>
              <div style={{ height: "40px" }}>
                {steckbrief && (
                  <a
                    href={steckbrief}
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <InfoLogo src={logoInfo} alt='Link zu Baumsteckbrief' />
                  </a>
                )}
                {gattungwikipedia && (
                  <a
                    href={gattungwikipedia}
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <WikipediaLogo src={logoWikipedia} alt='Link zu Wikipedia-Eintrag' />
                  </a>
                )}
                {gattungwikicommons && (
                  <a
                    href={gattungwikicommons}
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <WikicommonsLogo src={logoWikicommons} alt='Link zu Wikicommons-Eintrag' />
                  </a>
                )}
                {gattungwikidata && (
                  <a
                    href={gattungwikidata}
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <WikidataLogo src={logoWikidata} alt='Link zu Wikidata-Eintrag' />
                  </a>
                )}
              </div>
            </InfoValue>
          </InfoContainer>
        )}
        {gattung && (
          <InfoContainer>
            <span>Gattung (wiss.)</span>
            <InfoValue>{gattung}</InfoValue>
          </InfoContainer>
        )}
        {baumhoehe && (
          <InfoContainer>
            <span>Höhe</span>
            <InfoValue>{formatNumber(baumhoehe)} m</InfoValue>
          </InfoContainer>
        )}
        {stammumfg && (
          <InfoContainer>
            <span>Stammumfang</span>
            <InfoValue>{formatNumber(stammumfg)} cm</InfoValue>
          </InfoContainer>
        )}
        {kronedurch && (
          <InfoContainer>
            <span>Kronendurchmesser</span>
            <InfoValue>{formatNumber(kronedurch)} m</InfoValue>
          </InfoContainer>
        )}
        {eigentuemer && (
          <InfoContainer>
            <span>Eigentümer</span>
            <InfoValue>{eigentuemer}</InfoValue>
          </InfoContainer>
        )}
        {kennzeich && (
          <InfoContainer>
            <span>Kennzeichen</span>
            <InfoValue>{kennzeich}</InfoValue>
          </InfoContainer>
        )}
        {gebiet && (
          <InfoContainer>
            <span>Gebiet</span>
            <InfoValue>{gebiet}</InfoValue>
          </InfoContainer>
        )}
        {status_patenbaum && (
          <InfoContainer>
            <span>Patenbaum-Status</span>
            <InfoValue><a target="_blank" href="https://www.leipzig.de/freizeit-kultur-und-tourismus/parks-waelder-und-friedhoefe/spenden-und-patenschaften/baumstarke-stadt">{status_patenbaum}</a>{patenschaftsnummer ? ": " + patenschaftsnummer : ""}</InfoValue>
          </InfoContainer>
        )}
        {letzte_bewaesserung && (
          <InfoContainer>
            <span>Letzte städtische Bewässerung</span>
            <InfoValue>{letzte_bewaesserung}</InfoValue>
          </InfoContainer>
        )}
        {standortnr && (
          <InfoContainer>
            <span>Standortnummer</span>
            <InfoValue>{standortnr}</InfoValue>
          </InfoContainer>
        )}
        {treeAge && (
          <>
            <InfoContainer>
              <span>Standalter</span>
              <InfoValue>{treeAge} Jahre</InfoValue>
            </InfoContainer>
            <ExpandablePanel
              title={
                <>
                  <span style={{ marginRight: 8 }}>Wasserbedarf:</span>
                  <WaterDrops dropsAmount={getWaterNeedByAge(treeAge)} />
                </>
              }
            >
              <WaterNeedsInfo />
            </ExpandablePanel>
          </>
        )}
        { notes && (
          <ExpandablePanel
            title={
              <>
                <span style={{ marginRight: 8 }}>Hintergrund-Infos</span>
              </>
            }
          >
            <span dangerouslySetInnerHTML={{ __html: `${notes}` }}></span>
          </ExpandablePanel>
        )}
        <ExpandablePanel
          title={
            <>
              <div>Wassermenge</div>
              <SmallParagraph>der letzten 30 Tage</SmallParagraph>
              <div>
                <BaselineGrid>
                  <span>{wateredCircle}</span>
                  <SmallParagraph>
                    Gießungen: {wateringsSum.toFixed(1)}l
                  </SmallParagraph>
                </BaselineGrid>
              </div>
              <div>
                <BaselineGrid>
                  <span>{rainCircle}</span>
                  <SmallParagraph>Regen: {rainSum.toFixed(1)}l</SmallParagraph>
                </BaselineGrid>
              </div>
            </>
          }
          isExpanded
        >
          <StackedBarChart
            selectedTreeData={selectedTreeData}
            // We set the date here to 15:00 because all waterings from now on will have a timestamp of 15:00.
            date={barChartDate}
          />
        </ExpandablePanel>
        {Array.isArray(waterings) && waterings.length > 0 && (
          <ExpandablePanel
            isExpanded={true}
            title={
              <>
                Letzte Bewässerungen
                <SmallParagraph>Neueste zuerst</SmallParagraph>
              </>
            }
          >
            <UsersWateringsList
              waterings={waterings}
              treeId={selectedTreeData.id}
            />
          </ExpandablePanel>
        )}

        <br />
        {!userData && (
          <div>
            <Login />
            <ParticipateButton />
          </div>
        )}

        {userData && !userData.isVerified && (
          <>
            <Paragraph>
              Bäume adoptieren und wässern ist nur möglich mit verifiziertem
              Account.
            </Paragraph>
            <NonVerfiedMailMessage />
          </>
        )}

        {userData && userData.isVerified && (
          <ActionsWrapper>
            <ButtonWater />
            <ButtonRound
              margin='15px 0'
              onClick={() =>
                adoptedByLoggedInUser ? unadoptTree() : adoptTree()
              }
              type='secondary'
            >
              {adoptedByLoggedInUser &&
                !isBeingUnadopted &&
                'Adoption aufheben'}
              {adoptedByLoggedInUser &&
                isBeingUnadopted &&
                'Adoption wird aufgehoben'}
              {!adoptedByLoggedInUser && !isBeingAdopted && 'Baum adoptieren'}
              {!adoptedByLoggedInUser &&
                isBeingAdopted &&
                'Baum wird adoptiert'}
            </ButtonRound>
            <ParticipateButton />
          </ActionsWrapper>
        )}
      </FlexColumnDiv>
    </Wrapper>
  );
};

export default TreeInfos;
