import { QueryFunction, useQuery } from 'react-query';
import { ExtendedFeatureCollection } from 'd3-geo';
import { loadPumpsData } from '../requests/loadPumpsData';

const loadData: QueryFunction = async (): Promise<ExtendedFeatureCollection> => {
  return await loadPumpsData();
};

export const usePumpsGeoJson = (): {
  isLoading: boolean;
  data: ExtendedFeatureCollection | null;
  error: Error | null;
} => {
  const dataParams = 'pumps-geojson';
  const { isLoading, data, error } = useQuery<unknown, Error, ExtendedFeatureCollection>(
    dataParams,
    loadData,
    { staleTime: Infinity }
  );

  return {
    isLoading,
    data: data || null,
    error: error || null,
  };
};
