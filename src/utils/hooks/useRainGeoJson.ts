import { QueryFunction, useQuery } from 'react-query';
import { ExtendedFeatureCollection } from 'd3-geo';
import { loadRainGeoJson } from '../requests/loadRainGeoJson';

const loadData: QueryFunction = async (): Promise<ExtendedFeatureCollection> => {
  return await loadRainGeoJson();
};

export const useRainGeoJson = (): {
  isLoading: boolean;
  data: ExtendedFeatureCollection | null;
  error: Error | null;
} => {
  const dataParams = 'rain-geojson';
  const { isLoading, data, error } = useQuery<unknown, Error, ExtendedFeatureCollection>(
    dataParams,
    loadData,
    { staleTime: Infinity }
  );

  return {
    isLoading,
    data: data || null,
    error: error || null,
  };
};
