import { QueryFunction, useQuery } from 'react-query';
import { loadTreesArrow } from '../requests/loadTreesArrow';

const loadData: QueryFunction = async (): Promise<Blob> => {
  return await loadTreesArrow();
};

export const useTreesArrow = (): {
  isLoading: boolean;
  data: Blob | null;
  error: Error | null;
} => {
  const dataParams = 'trees-arrow';
  const { isLoading, data, error } = useQuery<unknown, Error, Blob>(
    dataParams,
    loadData,
    { staleTime: Infinity }
  );

  return {
    isLoading,
    data: data || null,
    error: error || null,
  };
};
