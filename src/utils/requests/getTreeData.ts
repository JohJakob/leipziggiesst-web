import { RawWateringType, SelectedTreeType } from '../../common/interfaces';
import { createClient } from './supabase/client'

interface TreeWateringsResponse {
  data: RawWateringType[] | undefined;
}


const calcuateRadolan = (radolanDays: number): number => radolanDays / 10;

export const getTreeData = async (
  id: string
): Promise<{
  selectedTreeData: SelectedTreeType | undefined;
}> => {
  const supabase = createClient()
	const { data: treeData, error: treeError } = await supabase.from("trees_view").select("*").eq("id", id);
  const { data: wateredData, error: wateredError } = await supabase
		.from("trees_watered")
		.select("watering_id,timestamp,amount,username,tree_id")
		.eq("tree_id", id)
		.range(0, 25)
		.order("timestamp", { ascending: false });

  const resSelectedTree = treeData && treeData.length > 0 && treeData[0];

  const selectedTreeData = resSelectedTree && {
      id: resSelectedTree["id"],
      lat: resSelectedTree["lat"],
      lng: resSelectedTree["lng"],
      artdtsch: resSelectedTree["artdtsch"],
      artbot: resSelectedTree["artbot"],
      gattungdeutsch: resSelectedTree["gattungdeutsch2"],
      gattung: resSelectedTree["gattung"],
      gattungwikipedia: resSelectedTree["gattungwikipedia"],
      gattungwikidata: resSelectedTree["gattungwikidata"],
      gattungwikicommons: resSelectedTree["gattungwikicommons"],
      artwikipedia: resSelectedTree["artwikipedia"],
      artwikidata: resSelectedTree["artwikidata"],
      artwikicommons: resSelectedTree["artwikicommons"],
      strname: resSelectedTree["strname"],
      hausnr: resSelectedTree["hausnr"],
      pflanzjahr: resSelectedTree["pflanzjahr"],
      kronedurch: resSelectedTree["kronedurch"],
      stammumfg: resSelectedTree["stammumfg"],
      baumhoehe: resSelectedTree["baumhoehe"],
      bezirk: resSelectedTree["bezirk"],
      gebiet: resSelectedTree["gebiet"],
      status_patenbaum: resSelectedTree["status_patenbaum"],
      patenschaftsnummer: resSelectedTree["patenschaftsnummer"],
      letzte_bewaesserung: resSelectedTree["letzte_bewaesserung"],
      adopted: resSelectedTree["adopted"],
      watered: resSelectedTree["watered"],
      radolan_sum: calcuateRadolan(resSelectedTree["radolan_sum"]),
      radolan_days: (resSelectedTree["radolan_days"] || []).map(calcuateRadolan),
      standortnr: resSelectedTree["standortnr"],
      notes: resSelectedTree["notes"],
      latitude: parseFloat(resSelectedTree["lat"]),
      longitude: parseFloat(resSelectedTree["lng"]),
      waterings: [],
      isAdopted: resSelectedTree["adopted"]

//      "aend_dat": resSelectedTree["zuletztakt"],
  }

  return Promise.resolve({selectedTreeData});
};
