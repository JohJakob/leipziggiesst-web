

export const loadTreesArrow = async (): Promise<Blob> => {
  if(!process.env.NEXT_PUBLIC_TREE_DATA_URL) throw new Error("env var NEXT_PUBLIC_TREE_DATA_URL should be defined in your environment");
  try {
    const response = await fetch(process.env.NEXT_PUBLIC_TREE_DATA_URL);
    if (!response.ok) {
      const msg = await response.text();
      throw new Error(msg);
    }
    return response.blob();
  } catch (err) {
    throw new Error(err as string);
  }
};
