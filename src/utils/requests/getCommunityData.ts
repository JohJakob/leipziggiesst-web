import { CommunityDataType } from '../../common/interfaces';
import { createAPIUrl } from '../createAPIUrl';
import { requests } from '../requestUtil';
import { createClient } from './supabase/client'

interface RawRequestResponse<DataType> {
  data: DataType;
}

type WateredAndAdoptedResponseType = RawRequestResponse<
  {
    tree_id: string;
    adopted: number;
    watered: number;
  }[]
>;

export const getCommunityData = async (): Promise<CommunityDataType> => {
  const supabase = createClient()

  const offset = 0;
  const limit = 10;
	const { data, error } = await supabase
		.rpc("get_watered_and_adopted")
		.range(offset, offset + (limit - 1))
		.order("tree_id", { ascending: true });

  const defaultCommunityData: CommunityDataType = {
    communityFlagsMap: {},
    wateredTreesIds: [],
    adoptedTreesIds: {},
  };

  if (!data) return defaultCommunityData;

  const newState = data.reduce(
    (acc: CommunityDataType, { tree_id: id, adopted, watered }) => {
      const isAdopted = adopted !== 0;
      const isWatered = watered !== 0;

      const newCommunityFlagsMap = acc.communityFlagsMap || {};
      newCommunityFlagsMap[id] = { isAdopted, isWatered };

      const newWateredTreesIds = acc.wateredTreesIds;
      if (isWatered) newWateredTreesIds.push(id);

      const newAdoptedTreesIds = acc.adoptedTreesIds;
      if (isAdopted) newAdoptedTreesIds[id] = adopted;
      return {
        communityFlagsMap: newCommunityFlagsMap,
        wateredTreesIds: newWateredTreesIds,
        adoptedTreesIds: newAdoptedTreesIds,
      };
    },
    defaultCommunityData
  );
  return newState;
};
